package br.com.itau;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MãoDoJogador mao = new MãoDoJogador();
        Baralho baralho = new Baralho();
        baralho.inicializaBaralho();


        int soma = 0;
        String comprar = "S";
        while (comprar.toLowerCase().equals("s") && soma <= 21){

            String cartasorteada = baralho.sortear();
            int valor = baralho.buscaValor(cartasorteada);
            baralho.deletaCarta(cartasorteada);

            //soma = mao.somaMao(valor);
            soma = soma + valor;

            System.out.println("carta sorteada: " + cartasorteada);
            System.out.println("valor da carta: " + valor);
            System.out.println("soma: " + soma);

            System.out.print("Deseja comprar mais uma carta? (S/N): ");
            comprar = scanner.nextLine();

        }

    }
}
